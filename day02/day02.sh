#!/usr/bin/env bash

# License MIT
# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
#
# Advent of Code 2021, Day 2 - Dive!
#
# https://adventofcode.com/2021/day/2

echo -e "==== sample data ===="
racket day02.rkt < day02.sample.in

echo -e "\n==== real data ===="
racket day02.rkt < day02.in
