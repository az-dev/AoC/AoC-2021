#!/usr/bin/env bash

# License MIT
# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
#
# Advent of Code 2021, Day 3 - Binary Diagnostic
#
# https://adventofcode.com/2021/day/3

echo -e "==== sample data ===="
racket day03.rkt < day03.sample.in

echo -e "\n==== real data ===="
racket day03.rkt < day03.in
