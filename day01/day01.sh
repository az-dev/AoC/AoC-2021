#!/usr/bin/env bash

# License MIT
# Copyright (c) 2021 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
#
# Advent of Code 2021, Day 1 - Sonar Sweep
#
# https://adventofcode.com/2021/day/1

echo -e "==== sample data ===="
racket day01.rkt < day01.sample.in

echo -e "\n==== real data ===="
racket day01.rkt < day01.in
